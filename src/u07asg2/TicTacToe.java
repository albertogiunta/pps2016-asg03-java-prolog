package u07asg2;

import java.util.List;
import java.util.Optional;

enum Player {
    PlayerX("X", "PlayerO") , PlayerO("O", "PlayerX");

    String string;
    String next;

    Player(String string, String next) {
        this.string = string;
        this.next = next;
    }

    public String getString() {
        return this.string;
    }

    public Player getNext() {
        return valueOf(this.next);
    }
}

public interface TicTacToe {

    void createBoard();

    List<Optional<Player>> getBoard();

    boolean checkCompleted();

    Optional<Player> checkVictory();

    boolean move(Player player, int i, int j);

    int winCount(Player current, Player winner);
}
