package u07asg;


import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.Optional;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class TicTacToeApp {

    private static final int ROWS = 4;
    private static final int COLS = 4;

    private final TicTacToe ttt;
    private final JButton[][] board    = new JButton[ROWS][COLS];
    private final JButton     exit     = new JButton("Exit");
    private final JFrame      frame    = new JFrame("TTT");
    private       boolean     finished = false;
    private       Player      turn     = Player.PlayerX;
    private       int         moves    = 0;

    private void changeTurn() {
        System.out.println(this.turn);
//        turn = turn.getNext();
        turn = turn.getRandom();
    }

    public TicTacToeApp(TicTacToe ttt) throws Exception {
        this.ttt = ttt;
        initPane();
    }

    private void humanMove(int i, int j) {
        System.out.println(ttt.toString());
        if (ttt.move(turn, i, j)) {
            board[i][j].setText(turn.getString());
            moves++;
            changeTurn();
            if (moves > 10) {
                System.out.println("X winning count: " + ttt.winCount(turn, Player.PlayerX));
                System.out.println("O winning count: " + ttt.winCount(turn, Player.PlayerO));
                System.out.println("Z winning count: " + ttt.winCount(turn, Player.PlayerZ));
            }
        }
        Optional<Player> victory = ttt.checkVictory();
        if (victory.isPresent()) {
            exit.setText(victory.get() + " won!");
            finished = true;
            return;
        }
        if (ttt.checkCompleted()) {
            exit.setText("Even!");
            finished = true;
            return;
        }
    }

    private void initPane() {
        frame.setLayout(new BorderLayout());
        JPanel b = new JPanel(new GridLayout(ROWS, COLS));
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                final int i2 = i;
                final int j2 = j;
                board[i][j] = new JButton("");
                b.add(board[i][j]);
                board[i][j].addActionListener(e -> {
                    if (!finished) humanMove(i2, j2);
                });
            }
        }
        JPanel s = new JPanel(new FlowLayout());
        s.add(exit);
        exit.addActionListener(e -> System.exit(0));
        frame.add(BorderLayout.CENTER, b);
        frame.add(BorderLayout.SOUTH, s);
        frame.setSize(400, 430);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        try {
            new TicTacToeApp(new TicTacToeImpl("src/u07asg/ttt.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
        }
    }
}
