% symbol(+OnBoard,-OnScreen): symbols for the board and their rendering
symbol(null,'_').
symbol(p1,'X').
symbol(p2,'O').

% result(+OnBoard,-OnScreen): result of a game
result(even,"even!").
result(p1,"player 1 wins").
result(p2,"player 2 wins").

% other_player(?Player,?OtherPlayer)
other_player(p1,p2).
other_player(p2,p1).

% render(+List): prints a TTT table (9 elements list) on console
render(L) :- convert_symbols(L,[A,B,C,D,E,F,G,H,I]),print_row(A,B,C),print_row(D,E,F),print_row(G,H,I).
convert_symbols(L,L2) :- findall(R,(member(X,L),symbol(X,R)),L2).
print_row(A,B,C) :- put(A),put(' '),put(B),put(' '),put(C),nl.

% render(+List,+Result): prints a TTT table plus result
render_full(L,Result) :- result(Result,OnScreen),print(OnScreen),nl,render(L),nl,nl.

% create_board(-Board): creates an initially empty board
create_board(B):-create_list(9,null,B).
create_list(0,_,[]) :- !.
create_list(N,X,[X|T]) :- N2 is N-1, create_list(N2,X,T).

% next_board(+Board,+Player,?NewBoard): finds (zero, one or many) new boards as Player moves
next_board([null|B],PL,[PL|B]).
next_board([X|B],PL,[X|B2]):-next_board(B,PL,B2).

% final(+Board,-Result): checks where the board is final and why
final(B,p1) :- finalpatt(P), match(B,P,p1), !.
final(B,p2) :- finalpatt(P), match(B,P,p2), !.
final(B,even) :- not(member(null,B)).

% nth_pos(+Board, +Divisor, +Mod result, -Result): puts each Nth element of a list inside another list
nth_pos(B, N, M, R):-
    nth_pos(B, 0, N, M, [], R).

nth_pos([], I, N, M, Acc, Acc).

nth_pos([H|T], I, N, M, Acc, R):-
    M =:= I mod N,
    append(Acc, [H], Acc2),
    I2 is I + 1,
    nth_pos(T, I2, N, M, Acc2, R).

nth_pos([H|T], I, N, M, Acc, R):-
	not(M =:= I mod N),
    I2 is I + 1,
    nth_pos(T, I2, N, M, Acc, R).

% accumulate(+Board, -Result): creates and appends the columns to a list of rows
accumulate(L, RES) :- nth_pos(L, 3, 0, L2), 
                    nth_pos(L, 3, 1, L3), 
                    nth_pos(L, 3, 2, L4), 
                    append(L, L2, R),
                    append(R, L3, R2),
                    append(R2, L4, RES).

% search_revenge(+Loser, +Board): searches for revenge patterns
search_revenge(LOS, [null,LOS,LOS|_]).
search_revenge(LOS, [LOS,null,LOS|_]).
search_revenge(LOS, [LOS,LOS,null|_]).
search_revenge(LOS, [_|[H|[H2|T]]]) :- search_revenge(LOS, T).

% make_list_and_search_revenge(+Loser, +Board): first it appends the columns to the rows, then searches revenge patterns
make_list_and_search_revenge(LOS, L) :- accumulate(L, RES), search_revenge(LOS, RES).

check_revenge(B,p1) :- ((finalpatt(P), match(B,P,p1), not(make_list_and_search_revenge(p2, B))) ; (finalpatt(P), match(B,P,p2), make_list_and_search_revenge(p1, B))).
check_revenge(B,p2) :- ((finalpatt(P), match(B,P,p2), not(make_list_and_search_revenge(p1, B))) ; (finalpatt(P), match(B,P,p1), make_list_and_search_revenge(p2, B))).

% match(Board,Pattern,Player): checks if in the board, the player matches a winning pattern
match([],[],_).
match([M|B],[x|P],M):-match(B,P,M).
match([_|B],[o|P],M):-match(B,P,M).

% finalpatt(+Pattern): gives a winning pattern
finalpatt([x,x,x,o,o,o,o,o,o]).
finalpatt([o,o,o,x,x,x,o,o,o]).
finalpatt([o,o,o,o,o,o,x,x,x]).
finalpatt([x,o,o,x,o,o,x,o,o]).
finalpatt([o,x,o,o,x,o,o,x,o]).
finalpatt([o,o,x,o,o,x,o,o,x]).
finalpatt([x,o,o,o,x,o,o,o,x]).
finalpatt([o,o,x,o,x,o,x,o,o]).

finalpatt2([null,x,x,o,o,o,o,o,o]).
finalpatt2([x,null,x,o,o,o,o,o,o]).
finalpatt2([x,x,null,o,o,o,o,o,o]).
finalpatt2([o,o,o,null,x,x,o,o,o]).
finalpatt2([o,o,o,x,null,x,o,o,o]).
finalpatt2([o,o,o,x,x,null,o,o,o]).
finalpatt2([o,o,o,o,o,o,null,x,x]).
finalpatt2([o,o,o,o,o,o,x,null,x]).
finalpatt2([o,o,o,o,o,o,x,x,null]).

finalpatt2([null,o,o,x,o,o,x,o,o]).
finalpatt2([x,o,o,null,o,o,x,o,o]).
finalpatt2([x,o,o,x,o,o,null,o,o]).
finalpatt2([o,null,o,o,x,o,o,x,o]).
finalpatt2([o,x,o,o,null,o,o,x,o]).
finalpatt2([o,x,o,o,x,o,o,null,o]).
finalpatt2([o,o,null,o,o,x,o,o,x]).
finalpatt2([o,o,x,o,o,null,o,o,x]).
finalpatt2([o,o,x,o,o,x,o,o,null]).

% game(+Board,+Player,-FinalBoard,-Result): finds one (zero, one or many) final boards and results
game(B,_,B,Result) :- final(B,Result),!.
game(B,PL,BF,Result):- next_board(B,PL,B2), other_player(PL,PL2),game(B2,PL2,BF,Result).

% statistics(+Board,+Player,+Result,-Count): counts how many time Res will happen 
statistics(B,P,Res,Count) :- findall(a, game(B,P,_,Res),L), length(L,Count).