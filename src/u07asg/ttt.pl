% symbol(+OnBoard,-OnScreen): symbols for the board and their rendering
symbol(null,'_').
symbol(p1,'X').
symbol(p2,'O').
symbol(p3,'Z').

% result(+OnBoard,-OnScreen): result of a game
result(even,"even!").
result(p1,"player 1 wins").
result(p2,"player 2 wins").
result(p3,"player 3 wins").

% other_player(?Player,?OtherPlayer)
other_player(p1,p2).
other_player(p2,p3).
other_player(p3,p1).
%other_player(p2,p3).
%other_player(p3,p1).
%other_player(p3,p2).

% render(+List): prints a TTT table (9 elements list) on console
render(L) :- convert_symbols(L,[A,B,C,D,E,F,G,H,I,LL,M,N,O,P,Q,RR]),print_row(A,B,C,D),print_row(E,F,G,H),print_row(I,LL,M,N),print_row(O,P,Q,RR).
convert_symbols(L,L2) :- findall(R,(member(X,L),symbol(X,R)),L2).
print_row(A,B,C,D) :- put(A),put(' '),put(B),put(' '),put(C),put(' '),put(D),nl.

% render(+List,+Result): prints a TTT table plus result
render_full(L,Result) :- result(Result,OnScreen),print(OnScreen),nl,render(L),nl,nl.

% create_board(-Board): creates an initially empty board
create_board(B):-create_list(16,null,B).
create_list(0,_,[]) :- !.
create_list(N,X,[X|T]) :- N2 is N-1, create_list(N2,X,T).

% next_board(+Board,+Player,?NewBoard): finds (zero, one or many) new boards as Player moves
next_board([null|B],PL,[PL|B]).
next_board([X|B],PL,[X|B2]):-next_board(B,PL,B2).

% final(+Board,-Result): checks where the board is final and why
final(B,p1) :- finalpatt(P), match(B,P,p1),!.
final(B,p2) :- finalpatt(P), match(B,P,p2),!.
final(B,p3) :- finalpatt(P), match(B,P,p3),!.
final(B,even) :- not(member(null,B)).

% match(Board,Pattern,Player): checks if in the board, the player matches a winning pattern
match([],[],_).
match([M|B],[x|P],M):-match(B,P,M).
match([_|B],[o|P],M):-match(B,P,M).

% finalpatt(+Pattern): gives a winning pattern
finalpatt([x,x,x,o, o,o,o,o, o,o,o,o, o,o,o,o ]).
finalpatt([o,x,x,x, o,o,o,o, o,o,o,o, o,o,o,o ]).

finalpatt([o,o,o,o, x,x,x,o, o,o,o,o, o,o,o,o ]).
finalpatt([o,o,o,o, o,x,x,x, o,o,o,o, o,o,o,o ]).

finalpatt([o,o,o,o, o,o,o,o, x,x,x,o, o,o,o,o ]).
finalpatt([o,o,o,o, o,o,o,o, o,x,x,x, o,o,o,o ]).

finalpatt([o,o,o,o, o,o,o,o, o,o,o,o, x,x,x,o ]).
finalpatt([o,o,o,o, o,o,o,o, o,o,o,o, o,x,x,x ]).

finalpatt([x,o,o,o, x,o,o,o, x,o,o,o, o,o,o,o ]).
finalpatt([o,o,o,o, x,o,o,o, x,o,o,o, x,o,o,o ]).

finalpatt([o,x,o,o, o,x,o,o, o,x,o,o, o,o,o,o ]).
finalpatt([o,o,o,o, o,x,o,o, o,x,o,o, o,x,o,o ]).

finalpatt([o,o,x,o, o,o,x,o, o,o,x,o, o,o,o,o ]).
finalpatt([o,o,o,o, o,o,x,o, o,o,x,o, o,o,x,o ]).

finalpatt([o,o,o,x, o,o,o,x, o,o,o,x, o,o,o,o ]).
finalpatt([o,o,o,o, o,o,o,x, o,o,o,x, o,o,o,x ]).

finalpatt([x,o,o,o, o,x,o,o, o,o,x,o, o,o,o,o ]).
finalpatt([o,o,o,o, o,x,o,o, o,o,x,o, o,o,o,x ]).

finalpatt([o,o,o,x, o,o,x,o, o,x,o,o, o,o,o,o ]).
finalpatt([o,o,o,o, o,o,x,o, o,x,o,o, x,o,o,o ]).

finalpatt([o,o,o,o, x,o,o,o, o,x,o,o, o,o,x,o ]).
finalpatt([o,x,o,o, o,o,x,o, o,o,o,x, o,o,o,o ]).

finalpatt([o,o,o,o, o,o,o,x, o,o,x,o, o,x,o,o ]).
finalpatt([o,o,x,o, o,x,o,o, x,o,o,o, o,o,o,o ]).

% game(+Board,+Player,-FinalBoard,-Result): finds one (zero, one or many) final boards and results
game(B,_,B,Result) :- final(B,Result),!.
game(B,PL,BF,Result):- next_board(B,PL,B2), other_player(PL,PL2),game(B2,PL2,BF,Result).

% statistics(+Board,+Player,+Result,-Count): counts how many time Res will happen
statistics(B,P,Res,Count) :- findall(a, game(B,P,_,Res),L), length(L,Count). 