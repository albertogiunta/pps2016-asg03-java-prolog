package u07asg2

import java.util.Optional

import alice.tuprolog._

object Scala2P {

     def extractTerm(solveInfo: SolveInfo, i: Integer): Term =
          solveInfo.getSolution.asInstanceOf[Struct].getArg(i).getTerm

     def extractTerm(solveInfo: SolveInfo, s: String): Term =
          solveInfo.getTerm(s)


     implicit def stringToTerm(s: String): Term = Term.createTerm(s)

     implicit def seqToTerm[T](s: Seq[T]): Term = s.mkString("[", ",", "]")

     implicit def stringToTheory[T](s: String): Theory = new Theory(s)

     def mkPrologEngine(theory: Theory): Term => Stream[SolveInfo] = {
          val engine = new Prolog
          engine.setTheory(theory)

          goal =>
               new Iterable[SolveInfo] {

                    override def iterator = new Iterator[SolveInfo] {
                         var solution: Option[SolveInfo] = Some(engine.solve(goal))

                         override def hasNext = solution.isDefined &&
                             (solution.get.isSuccess || solution.get.hasOpenAlternatives)

                         override def next() =
                              try solution.get
                              finally solution = if (solution.get.hasOpenAlternatives) Some(engine.solveNext()) else None
                    }
               }.toStream
     }

     def solveWithSuccess(engine: Term => Stream[SolveInfo], goal: Term): Boolean =
          engine(goal).map(_.isSuccess).headOption == Some(true)

     def solveOneAndGetTerm(engine: Term => Stream[SolveInfo], goal: Term, term: String): Term =
          engine(goal).headOption map (extractTerm(_, term)) get
}


object TryScala2P extends App {

     val ttt = new TicTacToeImpl("src/u07asg2/ttt.pl")
     ttt.createBoard()
//     println(ttt.checkCompleted())

     ttt.move(Player.PlayerO, 0, 0)
     ttt.move(Player.PlayerO, 0, 1)
     ttt.move(Player.PlayerO, 0, 2)
     ttt.move(Player.PlayerX, 1, 0)
     ttt.move(Player.PlayerX, 1, 1)
//     ttt.move(Player.PlayerO, 1, 2)
     ttt.move(Player.PlayerO, 2, 0)
     ttt.move(Player.PlayerO, 2, 1)
     ttt.move(Player.PlayerX, 2, 2)

//     println(ttt.getBoard())
     println(ttt.winCount(Player.PlayerO, Player.PlayerO))
     println(ttt.winCount(Player.PlayerO, Player.PlayerX))
     println(ttt.winCount(Player.PlayerX, Player.PlayerX))
     println(ttt.winCount(Player.PlayerX, Player.PlayerO))
     val victory = ttt.checkVictory
     if (victory.isPresent) {
          println(victory.get + " won!")
     }
     if (ttt.checkCompleted) {
          println("Even!")
     }

}
