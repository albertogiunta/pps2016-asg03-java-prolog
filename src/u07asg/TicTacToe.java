package u07asg;

import java.util.List;
import java.util.Optional;
import java.util.Random;

enum Player {
    PlayerX("X", "PlayerO") , PlayerO("O", "PlayerZ"), PlayerZ("Z", "PlayerX");

    String string;
    String next;

    Player(String string, String next) {
        this.string = string;
        this.next = next;
    }

    public String getString() {
        return this.string;
    }

    public Player getNext() {
        return valueOf(this.next);
    }

    public Player getNext(String next) {
        return valueOf(next);
    }

    public Player getRandom() {
        String player = "";
        switch (new Random().nextInt(3)) {
            case 0:
                player = "PlayerX";
                break;
            case 1:
                player = "PlayerO";
                break;
            case 2:
                player = "PlayerZ";
                break;
        }
        return getNext(player);
    }
}

public interface TicTacToe {

    void createBoard();

    List<Optional<Player>> getBoard();

    boolean checkCompleted();

    Optional<Player> checkVictory();

    boolean move(Player player, int i, int j);

    int winCount(Player current, Player winner);
}
